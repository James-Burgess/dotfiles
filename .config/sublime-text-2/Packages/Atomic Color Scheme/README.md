# Atomic colors for Sublime Text

Sublime-atomic is a dark & light color scheme designed to use on Sublime Text. It consists of sixteen colors selected procedurally (algorithms) and it's part of a bigger project: [Atomic](https://github.com/gerardbm/Atomic), which includes themes and color schemes for more code editors (Vim, Neovim and NetBeans), terminals (URxvt, XTerm, xfce4-terminal and konsole) and some terminal programs (tmux, zsh, irssi, cmus).

## Color Palette

Atomic comes with five modes of color and two contrasts for each one:

- Dark blue, soft contrast
- Dark blue, hard contrast
- Dark cyan, soft contrast
- Dark cyan, hard contrast
- Night orange, soft contrast
- Night orange, hard contrast
- Night red, soft contrast
- Night red, hard contrast
- Light sepia, soft contrast
- Light sepia, hard contrast

Dark blue:

![Atomic-scheme](https://raw.githubusercontent.com/gerardbm/Sublime-Atomic-Scheme/master/img/dark-blue.png)

Dark cyan:

![Atomic-scheme](https://raw.githubusercontent.com/gerardbm/Sublime-Atomic-Scheme/master/img/dark-cyan.png)

Night orange:

![Atomic-scheme](https://raw.githubusercontent.com/gerardbm/Sublime-Atomic-Scheme/master/img/night-orange.png)

Night red:

![Atomic-scheme](https://raw.githubusercontent.com/gerardbm/Sublime-Atomic-Scheme/master/img/night-red.png)

Light sepia:

![Atomic-scheme](https://raw.githubusercontent.com/gerardbm/Sublime-Atomic-Scheme/master/img/light.png)

## Screenshots

![Atomic-modes](https://raw.githubusercontent.com/gerardbm/Sublime-Atomic-Scheme/master/screenshots/atomic-modes.png)

## Installation
#### Package Control

1. Make sure you already have Package Control installed.
2. Choose `Install Package` from the Command Palette (`Ctrl+Shift+P` on Windows/Linux, `⇧⌘P` on OS X).
3. Select Atomic in the menu: `Preferences` > `Atomic` > `Atomic`.

With `auto_upgrade` enabled, Package Control will keep all installed packages up-to-date!

#### Manual Installation

1. Download this repository, unpack the downloaded file and manually place `Atomic.tmTheme` file into `~/.config/sublime-text-3/Packages/Atomic/`.
2. Select Atomic in the menu: `Preferences` > `Atomic` > `Atomic`.
